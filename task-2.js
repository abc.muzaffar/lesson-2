// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...

function fastFood(image, title, url, price, counter) {
  return {
    image,
    title,
    url,
    price,
    counter,
  };
}

// const evos = fastFood("lav.png", "lavash", "evos.uz", 12, 2);
// console.log(evos);


// Constructor Function
// You code here ...

function FastFood(image, title, url, price, counter) {
  (this.image = image),
    (this.title = title),
    (this.url = url),
    (this.price = price),
    (this.counter = counter);
}

// const lesAiles = new FastFood("don.svg", "donar", "lesailes.uz", 18, 4);
// console.log(lesAiles);